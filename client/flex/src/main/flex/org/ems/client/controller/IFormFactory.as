/*******************************************************************/
/*
	File: IFormFactory.as

	Author: James Cowan
*/
/*******************************************************************/

package org.ems.client.controller
{
	import mx.containers.TitleWindow;
	
	public interface IFormFactory
	{
		/*******************************************************************/
		/** getForm */
		
		function getForm(entityName:String):TitleWindow;

	} // IFormFactory

} // package

/*******************************************************************/
// End of file
/*******************************************************************/ 

