/*******************************************************************/
/*
	File: IEntityMetaDataService.as

	Author: James Cowan
*/
/*******************************************************************/

package org.ems.client.service
{		
	public interface IEntityMetaDataService
	{
		/*******************************************************************/
		/** getEntityMetaData */
		
		function getEntityMetaData():void;

	} // IEntityMetaDataService
	
} // package

/*******************************************************************/
// End of file
/*******************************************************************/

