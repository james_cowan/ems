/*******************************************************************/
/*
	File: IEntityFactory.as

	Author: James Cowan
*/
/*******************************************************************/

package org.ems.client.controller
{
	public interface IEntityFactory
	{
		/*******************************************************************/
		/** createEntity */
		
		function createEntity(entity:String):Object;

	} // IEntityFactory

} // package

/*******************************************************************/
// End of file
/*******************************************************************/ 

