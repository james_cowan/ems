/*******************************************************************/
/*
	File: IGenerator.java

	Author: James Cowan
*/
/*******************************************************************/

package org.ems.generator;

import java.io.IOException;

/*******************************************************************/
/** IGenerator */

public interface IGenerator
{
	/************************************************************/
	/** generate */

	public void generate(IGeneratorOutput output) throws IOException, GeneratorException;

} // IGenerator

/*******************************************************************/

