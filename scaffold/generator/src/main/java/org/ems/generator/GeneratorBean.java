/*******************************************************************/
/*
	File: GeneratorBean.java
*/
/*******************************************************************/

package org.ems.generator;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.ArrayList;

/*******************************************************************/
/** GeneratorBean */
/*******************************************************************/

public class GeneratorBean
{
	public GeneratorBean()
	{
	}

	/*******************************************************************/
	/* public methods */
	/*******************************************************************/
	/** getPropertyDescriptors */
	
	public PropertyDescriptor[] getPropertyDescriptors(String className,
							   String stopClassName)
	throws GeneratorException
	{
	Class<?> cl = getClass(className);
	Class<?> stopCl = cl.getSuperclass();
	
		if (!stopClassName.equals(""))
		{
			stopCl = getClass(stopClassName).getSuperclass();
		}
		
		return getPropertyDescriptors(getBeanInfo(cl, stopCl));
		
	} // getPropertyDescriptors
	
	/*******************************************************************/
	/** getPropertyDescriptors */
	
	public PropertyDescriptor[] getPropertyDescriptors(String className)

	throws GeneratorException
	{
		return getPropertyDescriptors(className, "");
		
	} // getPropertyDescriptors
	/*******************************************************************/
	/* protected methods */
	/*******************************************************************/
	/** getBeanInfo */
	
	protected BeanInfo getBeanInfo(Class<?> cl, Class<?> stopCl)
	throws GeneratorException
	{	
		try
		{
			return Introspector.getBeanInfo(cl, stopCl);
		}
		catch (IntrospectionException e)
		{
		String msg = "Cannot introspect class: "+cl.getName()+
			     " error: "+e.getMessage();
		
			throw new GeneratorException(msg);
		}
		
	} // getBeanInfo
	
	/*******************************************************************/
	/** getClass */
	
	protected Class<?> getClass(String className)
	throws GeneratorException
	{	
		try
		{
			return Class.forName(className);
		}
		catch (ClassNotFoundException ce)
		{
			throw new GeneratorException("Invalid class: "+className);
		}
		
	} // getClass
	
	/*******************************************************************/
	/** getPropertyDescriptors */
	
	protected PropertyDescriptor[] getPropertyDescriptors(BeanInfo info)
	{
	PropertyDescriptor props[] = info.getPropertyDescriptors();
	ArrayList<PropertyDescriptor> list = new ArrayList<PropertyDescriptor>();
		
		for (int count=0; count<props.length; count++)
		{
		PropertyDescriptor prop = props[count];
		
			if (prop.getReadMethod() == null || 
			    prop.getWriteMethod() == null)
			{
				continue;
			}
			list.add(prop);
		}
		
		PropertyDescriptor result[] = new PropertyDescriptor[list.size()];
		
		list.toArray(result);
		
		return result;
		
	} // getPropertyDescriptors
	
} // GeneratorBean

/*******************************************************************/


