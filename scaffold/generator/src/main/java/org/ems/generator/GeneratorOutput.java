/*******************************************************************/
/*
	File: GeneratorOutput.java
*/
/*******************************************************************/

package org.ems.generator;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

/*******************************************************************/
/** IGeneratorOutput */

public class GeneratorOutput implements IGeneratorOutput
{
	protected PrintStream					m_defaultOutput;
	protected Map<String, PrintStream>		m_outputMap;
	
	public GeneratorOutput(PrintStream defaultOutput, Map<String, PrintStream> outputMap)
	{
		m_defaultOutput = defaultOutput;
		m_outputMap = outputMap;		
	}
	
	public GeneratorOutput(PrintStream defaultOutput)
	{
		this(defaultOutput, new HashMap<String, PrintStream>());
	}
	
	/************************************************************/
	/** getOutput */

	public PrintStream getOutput()
	{
		return m_defaultOutput;
		
	} // getOutput
	
	/************************************************************/
	/** getOutput */

	public PrintStream getOutput(String context) throws GeneratorException
	{
	PrintStream output = m_outputMap.get(context);
	
		if (output == null)
		{
			throw new GeneratorException("No output for context: "+context);
		}
		
		return output;
		
	} // getOutput
	
	/************************************************************/
	/** getOutputContext */

	public Map<String, PrintStream> getOutputContext()
	{
		return m_outputMap;
		
	} // getOutputContext
	
	/************************************************************/
	/** setOutput */

	public void setOutput(String context, PrintStream out)
	{
		m_outputMap.put(context, out);
		
	} // setOutput

} // IGeneratorOutput

/*******************************************************************/

