/*******************************************************************/
/*
	File: IGeneratorOutput.java
*/
/*******************************************************************/

package org.ems.generator;

import java.io.PrintStream;
import java.util.Map;

/*******************************************************************/
/** IGeneratorOutput */

public interface IGeneratorOutput
{
	/************************************************************/
	/** getOutput */

	public PrintStream getOutput();
	
	/************************************************************/
	/** getOutput */

	public PrintStream getOutput(String context) throws GeneratorException;
	
	/************************************************************/
	/** getOutputContext */

	public Map<String, PrintStream> getOutputContext();
	
	/************************************************************/
	/** setOutput */

	public void setOutput(String context, PrintStream out);
	
} // IGeneratorOutput

/*******************************************************************/

