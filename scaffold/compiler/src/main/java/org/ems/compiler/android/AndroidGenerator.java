/************************************************************/
/*
	File: AndroidClassGenerator.java

	AndroidClassGenerator generates as Android parcel class from a java class.

	Author: James Cowan
*/
/************************************************************/

package org.ems.compiler.android;

import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import org.ems.compiler.ClassGenerator;
import org.ems.compiler.model.JavaClass;
import org.ems.compiler.model.JavaClassList;
import org.ems.compiler.model.TypeDescriptorList;
import org.ems.generator.GeneratorException;

/************************************************************/

public class AndroidGenerator extends ClassGenerator
{	
	protected JavaClassList m_javaClassList;
	
	public AndroidGenerator(JavaClassList javaClassList, JavaClass javaClass, Class<?> cl, TypeDescriptorList typeDescriptorList)
	throws GeneratorException
	{
		super(javaClass, cl, typeDescriptorList);
		
		m_javaClassList = javaClassList;
	}

	/************************************************************/
	/* public methods */
	/************************************************************/
	/** generate */

	public void generate(PrintStream out) throws IOException, GeneratorException
	{
	
	} // generate
	
	/************************************************************/
	/* protected methods */
	/************************************************************/
	/** addImport */
	
	protected void addImport(String propertyImport, Map<String, String> importTable, List<String> importList)
	{
		if (propertyImport.equals("") || importTable.get(propertyImport) != null)
		{
			return;
		}
			
		importTable.put(propertyImport, propertyImport);
		importList.add(propertyImport);
	}
	
	public void generateParcelMethods(PrintStream out) throws IOException, GeneratorException
	{
	List<PropertyDescriptor> propertyDescriptorList = getPropertyDescriptorList();
	String className = m_class.getSimpleName();
	String instanceName = getInstanceName(className);
	
		out.println("");
		out.println("\tprivate "+className+"(Parcel parcel)");
		out.println("\t{");
		if (!m_class.isEnum())
		{	
			out.println("\t\tthis();");
		}	
		out.println("\t\tthis.readFromParcel(parcel);");
		out.println("\t}");
		
		out.println("");
		out.println("\tpublic int describeContents()");
		out.println("\t{");
		out.println("\t\treturn 0;");
		out.println("\t}");
		
		out.println("");
		out.println("\tpublic void readFromParcel(Parcel parcel)");
		out.println("\t{");
		for (PropertyDescriptor descriptor : propertyDescriptorList)
		{		
			out.print("\t\t");
			generateParcelReadMethod(out, descriptor);
		}
		if (hasSuperClass())
		{
			out.println("\t\tsuper.readFromParcel(parcel);");
		}
		if (m_class.isEnum())
		{
			out.println("\t\tthis.value = parcel.readString();");
		}
		out.println("\t}");
		
		out.println("");
		out.println("\tpublic void writeToParcel(Parcel parcel, int flags)");
		out.println("\t{");
		for (PropertyDescriptor descriptor : propertyDescriptorList)
		{
			out.print("\t\t");
			generateParcelWriteMethod(out, descriptor);
		}
		if (hasSuperClass())
		{
			out.println("\t\tsuper.writeToParcel(parcel, flags);");
		}
		if (m_class.isEnum())
		{
			out.println("\t\tparcel.writeString(name());");
		}
		out.println("\t}");
		
		out.println("");
		out.println("\tpublic static final Parcelable.Creator<"+className+"> CREATOR = new Parcelable.Creator<"+className+">()");
		out.println("\t{");
		out.println("\t\tpublic "+className+" createFromParcel(Parcel source)");
		out.println("\t\t{");
		if (m_class.isEnum())
		{
			out.println("\t\t"+className+" "+instanceName+" = "+className+".valueOf(source.readString());");
			out.println("\t\t\treturn "+instanceName+";");
		}
		else
		{
			out.println("\t\t\treturn new "+className+"(source);");
		}
		out.println("\t\t}");
		out.println("\t\tpublic "+className+"[] newArray(int size)");
		out.println("\t\t{");
		out.println("\t\t\treturn new "+className+"[size];");
		out.println("\t\t}");
		out.println("\t};");
	}
	
	protected void generateParcelReadMethod(PrintStream out, PropertyDescriptor descriptor) throws IOException, GeneratorException
	{
	String instanceName = descriptor.getName();
	String typeName = getPropertyTypeName(descriptor);
	
		if (m_typeDescriptorMap.get(typeName) == null)
		{
			out.println("this."+instanceName+" = ("+typeName+") parcel.readParcelable("+typeName+".class.getClassLoader());");
			return;
		}
		
		switch (typeName)
		{
		default:
			throw new GeneratorException("Unknown type "+typeName);
			
		case "boolean":
		case "Boolean":
			out.println("this."+instanceName+" = parcel.readByte() == 1;");
			break;
		case "Date":
			out.println("this."+instanceName+" = parcel.readSerializable();");
			break;
		case "double":
		case "Double":
			out.println("this."+instanceName+" = parcel.readDouble();");
			break;
		case "float":
		case "Float":
			out.println("this."+instanceName+" = parcel.readFloat();");
			break;
		case "int":
		case "Integer":
			out.println("this."+instanceName+" = parcel.readInt();");
			break;
		case "List":
			out.println("parcel.readList(this."+instanceName+",List.class.getClassLoader());");
			break;
		case "long":
		case "Long":
			out.println("this."+instanceName+" = parcel.readLong();");
			break;	
		case "String":
			out.println("this."+instanceName+" = parcel.readString();");
			break;
		}
	}
	
	protected void generateParcelWriteMethod(PrintStream out, PropertyDescriptor descriptor) throws IOException, GeneratorException
	{
	String instanceName = descriptor.getName();
	String typeName = getPropertyTypeName(descriptor);
	
		if (m_typeDescriptorMap.get(typeName) == null)
		{
			out.println("parcel.writeParcelable("+instanceName+", flags);");
			return;
		}
		
		out.print("parcel.");
		
		switch (typeName)
		{
		default:
			throw new GeneratorException("Unknown type "+typeName);
			
		case "boolean":
		case "Boolean":
			out.println("writeByte((byte)(this."+instanceName+" ? 1 : 0));");
			break;
		case "Date":
			out.println("writeSerializable(this."+instanceName+");");
			break;
		case "double":
		case "Double":
			out.println("writeDouble(this."+instanceName+");");
			break;
		case "float":
		case "Float":
			out.println("writeFloat(this."+instanceName+");");
			break;
		case "int":
		case "Integer":
			out.println("writeInt(this."+instanceName+");");
			break;
		case "List":
			out.println("writeList(this."+instanceName+");");
			break;	
		case "long":
		case "Long":
			out.println("writeLong(this."+instanceName+");");
			break;	
		case "String":
			out.println("writeString(this."+instanceName+");");
			break;
		}
	}
	
	protected String getParmeterisedTypeName(PropertyDescriptor descriptor) 
	{
	Class<?> cl = descriptor.getPropertyType();
		
		if (cl.isArray())
		{
		Class<?> componentType = cl.getComponentType();
		String className = componentType.getSimpleName();
		
			return className+"[]";
		}
		
		if (cl == List.class)
		{
		Type type = descriptor.getReadMethod().getGenericReturnType();
		ParameterizedType pType = (ParameterizedType) type;
		Class<?> pClass = (Class<?>) pType.getActualTypeArguments()[0];
		
			return "List<"+pClass.getSimpleName()+">";
		}
		
		return super.getPropertyTypeName(descriptor);
	}

} // AndroidClassGenerator

/************************************************************/


