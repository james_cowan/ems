/************************************************************/
/*
	File: AndroidEnumGenerator.java

	AndroidEnumGenerator generates as parcelable Enum.

	Author: James Cowan
*/
/************************************************************/

package org.ems.compiler.swift;

import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import org.ems.compiler.model.JavaClass;
import org.ems.compiler.model.JavaClassList;
import org.ems.compiler.model.TypeDescriptorList;
import org.ems.generator.GeneratorException;

/************************************************************/

public class SwiftEnumGenerator extends SwiftGenerator
{
	private final static String STRING = "String";
	
	public SwiftEnumGenerator(JavaClassList javaClassList, JavaClass javaClass, Class<?> cl, TypeDescriptorList typeDescriptorList)
	throws GeneratorException
	{
		super(javaClassList, javaClass, cl, typeDescriptorList);
	}

	/************************************************************/
	/* public methods */
	/************************************************************/
	/** generate */

	public void generate(PrintStream out) throws IOException, GeneratorException
	{
	String className = m_class.getSimpleName();
	List<?> enumConstants = Arrays.asList(m_class.getEnumConstants());
	String enumValueName = m_javaClass.getEnumValue();
	Class<? extends Enum> enumClass = (Class<? extends Enum>) m_class;
	String enumType = getEnumType();

		out.println("public enum "+className+ " : "+enumType+" {");
		
		try 
		{
			for (Object enumConstant : enumConstants) {
				final Enum<?> enumValue = Enum.valueOf(enumClass, enumConstant.toString());
				Field f = m_class.getDeclaredField(enumValueName);
					
				f.setAccessible(true);	
				out.print("\tcase "+enumConstant+" = ");
				
				if (enumType.equals(STRING))
				{
					out.print("\"");
				}		
				out.print(f.get(enumValue));
				if (enumType.equals(STRING))
				{
					out.print("\"");
				}
				out.println("");
			}
			
		} catch (Exception e) {
			throw new GeneratorException(e.getMessage());
		}
		
		out.println("}");
	
	} // generate
	
	/************************************************************/
	/* protected methods */
	/************************************************************/	
	
	protected String getEnumType() throws GeneratorException
	{
	String enumValueName = m_javaClass.getEnumValue();
	
		if (enumValueName.equals("")) 
		{
			throw new GeneratorException("Must specify enumValueName");
		}

		try 
		{
		Field f = m_class.getDeclaredField(enumValueName);
		
			f.setAccessible(true);	
			
			String fType = f.getType().getSimpleName();
			
			switch (fType)
			{
			default:
				throw new GeneratorException("Enum type not supported "+fType);
				
			case STRING:
				return STRING;
			}
		} catch (Exception e) 
		{
			throw new GeneratorException("Cannot find enum type "+e.getMessage());
		}
	}

} // SwiftEnumGenerator

/************************************************************/


