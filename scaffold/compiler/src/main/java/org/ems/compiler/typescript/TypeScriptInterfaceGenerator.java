/************************************************************/
/*
	File: TypeScriptInterfaceGenerator.java

	TypeScriptInterfaceGenerator generates as TypeScript class file from a java class.

	Author: James Cowan
*/
/************************************************************/

package org.ems.compiler.typescript;

import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.ems.compiler.ClassGenerator;
import org.ems.compiler.model.ImportDescriptor;
import org.ems.compiler.model.JavaClass;
import org.ems.compiler.model.JavaClassList;
import org.ems.compiler.model.TypeDescriptorList;
import org.ems.generator.GeneratorException;

/************************************************************/

public class TypeScriptInterfaceGenerator extends TypeScriptGenerator
{	
	public TypeScriptInterfaceGenerator(JavaClassList javaClassList, JavaClass javaClass, Class<?> cl, TypeDescriptorList typeDescriptorList)
	throws GeneratorException
	{
		super(javaClassList, javaClass, cl, typeDescriptorList);
	}

	/************************************************************/
	/* public methods */
	/************************************************************/
	/** generate */

	public void generate(PrintStream out) throws IOException, GeneratorException
	{
	List<PropertyDescriptor> propertyDescriptorList = getPropertyDescriptorList();
	HashMap<String, ImportDescriptor> importTable = new HashMap<String, ImportDescriptor>();
	ArrayList<ImportDescriptor> importList = new ArrayList<ImportDescriptor>();
	String interfaceExtension = "";
	String className = m_class.getSimpleName();
	String interfaceName = ClassGenerator.INTERFACE+className;
	String tab = "";
	
		if (!m_javaClassList.getGroupClasses())
		{
		ImportDescriptor classImports[] = m_javaClass.getImportDescriptor();
			
			for (ImportDescriptor classImport : classImports)
			{
				addImport(classImport, importTable, importList);
			}
		}
	

		for (int count=0; count<propertyDescriptorList.size(); count++)
		{
		PropertyDescriptor propertyDescriptor = propertyDescriptorList.get(count);
		PropertyType propertyType = getPropertyType(propertyDescriptor);
		String moduleName = propertyType.moduleName;
		
			if (moduleName.equals(interfaceName))
			{
				continue;
			}
			
			if (!moduleName.equals("") && importTable.get(moduleName) == null)
			{
			ImportDescriptor importDescriptor = new ImportDescriptor();
				
				importDescriptor.setTypeName(moduleName);
				importDescriptor.setPath("./"+moduleName);
				addImport(importDescriptor, importTable, importList);
			}
		}
			
		if (hasSuperClass())
		{
		Class<?> superClass = m_class.getSuperclass();
		String superClassName = ClassGenerator.INTERFACE+superClass.getSimpleName();
		
			interfaceExtension = " extends "+superClassName;
			
			ImportDescriptor importDescriptor = new ImportDescriptor();
				
			importDescriptor.setTypeName(superClassName);
			importDescriptor.setPath("./"+superClassName);
			
			addImport(importDescriptor, importTable, importList);
		}
		
		if (importList.size() > 0 && !m_javaClassList.getGroupClasses())
		{		
			for (ImportDescriptor importDescriptor : importList)
			{	
				out.println("import "+importDescriptor.getTypeName()+" from \""+importDescriptor.getPath()+"\";");
			}
			
			if (importList.size() > 0) 
			{
				out.println("");
			}
		}
		
		out.println("export interface "+interfaceName+interfaceExtension+ " {");
		
		for (int count=0; count<propertyDescriptorList.size(); count++)
		{
		PropertyDescriptor propertyDescriptor = propertyDescriptorList.get(count);
		String name = propertyDescriptor.getName();
		PropertyType propertyType = getPropertyType(propertyDescriptor);
		
			out.println(tab+"\t"+name+":"+propertyType.typeName+";");
		}
		
		out.println(tab+"}");
	 
		if (!m_javaClassList.getGroupClasses()) 
		{
			out.println(tab+"export default "+ClassGenerator.INTERFACE+className+";");
		}
		else
		{
			out.println("");
		}

	} // generate
		
	/************************************************************/
	/* protected methods */
	/************************************************************/
		

} // TypeScriptInterfaceGenerator

/************************************************************/


