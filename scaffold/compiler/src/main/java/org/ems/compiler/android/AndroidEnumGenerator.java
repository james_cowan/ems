/************************************************************/
/*
	File: AndroidEnumGenerator.java

	AndroidEnumGenerator generates as parcelable Enum.

	Author: James Cowan
*/
/************************************************************/

package org.ems.compiler.android;

import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import org.ems.compiler.model.JavaClass;
import org.ems.compiler.model.JavaClassList;
import org.ems.compiler.model.TypeDescriptorList;
import org.ems.generator.GeneratorException;

/************************************************************/

public class AndroidEnumGenerator extends AndroidGenerator
{	
	public AndroidEnumGenerator(JavaClassList javaClassList, JavaClass javaClass, Class<?> cl, TypeDescriptorList typeDescriptorList)
	throws GeneratorException
	{
		super(javaClassList, javaClass, cl, typeDescriptorList);
	}

	/************************************************************/
	/* public methods */
	/************************************************************/
	/** generate */

	public void generate(PrintStream out) throws IOException, GeneratorException
	{
	String className = m_class.getSimpleName();
	List<?> enumConstants = Arrays.asList(m_class.getEnumConstants());
	String enumValueName = m_javaClass.getEnumValue();
	Class<? extends Enum> enumClass = (Class<? extends Enum>) m_class;
	String packageName = m_class.getPackage().getName();

		out.println("package "+packageName+";");
		out.println("");
		out.println("import android.os.Parcel;");
		out.println("import android.os.Parcelable;");

		out.println("public enum "+className+ " implements Parcelable");
		out.println("{");
		
		int count = 0;
		
		if (enumValueName.equals("")) {
			for (Object enumConstant : enumConstants) {
				out.print("\t"+enumConstant+"(\""+enumConstant+"\")");
				if (count < enumConstants.size()-1)
				{
					out.println(",");
				}
				count++;
			}
		}
		else {
			try {
				for (Object enumConstant : enumConstants) {
					final Enum<?> enumValue = Enum.valueOf(enumClass, enumConstant.toString());
					Field f = m_class.getDeclaredField(enumValueName);
						
					f.setAccessible(true);	
					out.print("\t"+enumConstant+"(\""+f.get(enumValue)+"\")");
					if (count < enumConstants.size()-1) {
						out.println(",");
					}
					count++;
				}
				
			} catch (Exception e) {
				throw new GeneratorException(e.getMessage());
			}	
		}
		
		out.println(";");
		out.println("");
		
		out.println("\tprivate String value;");
		out.println("");

	    out.println("\t"+className+"(String value)");
	    out.println("\t{");
	    out.println("\t\tthis.value = value;");
	    out.println("\t}");
	    out.println("");
	    
	    out.println("\tpublic String getValue()");
	    out.println("\t{");
	    out.println("\t\treturn this.value;");
	    out.println("\t}");
	    out.println("");
	    
	    out.println("\tpublic void setValue(String value)");
	    out.println("\t{");
	    out.println("\t\tthis.value = value;");
	    out.println("\t}");
	    out.println("");
	    
	    out.println("\tpublic String toString()");
	    out.println("\t{");
	    out.println("\t\treturn this.value;");
	    out.println("\t}");
	    
	    generateParcelMethods(out);

		out.println("");
		
		out.println("}");
	
	} // generate
	
	/************************************************************/
	/* protected methods */
	/************************************************************/
	

} // AndroidEnumGenerator

/************************************************************/


