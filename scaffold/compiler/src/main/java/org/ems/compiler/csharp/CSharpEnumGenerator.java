/************************************************************/
/*
	File: CSharpEnumGenerator.java

	Author: James Cowan
*/
/************************************************************/

package org.ems.compiler.csharp;

import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.ems.compiler.model.JavaClass;
import org.ems.compiler.model.JavaClassList;
import org.ems.compiler.model.TypeDescriptorList;
import org.ems.generator.GeneratorException;

/************************************************************/

public class CSharpEnumGenerator extends CSharpGenerator
{	
	private static final String	VALUE="value";
	
	public CSharpEnumGenerator(JavaClassList javaClassList, JavaClass javaClass, Class<?> cl, TypeDescriptorList typeDescriptorList)
	throws GeneratorException
	{
		super(javaClassList, javaClass, cl, typeDescriptorList);
	}

	/************************************************************/
	/* public methods */
	/************************************************************/
	/** generate */

	public void generate(PrintStream out) throws IOException, GeneratorException
	{
	List<String> importList = new ArrayList<String>();
	String className = m_class.getSimpleName();
	String instanceName = getInstanceName(className);
	List<?> enumConstants = Arrays.asList(m_class.getEnumConstants());
	String enumValueName = m_javaClass.getEnumValue();
	String nameSpace = m_javaClassList.getNameSpace();
	Class<? extends Enum> enumClass = (Class<? extends Enum>) m_class;
	
		addImport(importList);
	
		for (String importPath : importList)
		{
			out.println("using "+importPath+";");
		}
	
		if (importList.size() > 0)
		{
			out.println("");
		}

		out.println("namespace "+nameSpace);
		out.println("{");

		out.println("\tpublic class "+className);
		out.println("\t{");
		
		out.println("\t\tpublic string "+VALUE+"{ get;}");
		out.println("");
		
		if (enumValueName.equals("")) {
			for (Object enumConstant : enumConstants) {
				generateConstant(out, enumConstant.toString(), enumConstant.toString());
			}
		}
		else {
			try {
				for (Object enumConstant : enumConstants) {
					final Enum<?> enumValue = Enum.valueOf(enumClass, enumConstant.toString());
					Field f = m_class.getDeclaredField(enumValueName);
						
					f.setAccessible(true);
					generateConstant(out, enumConstant.toString(), f.get(enumValue).toString());					
				}
				
			} catch (Exception e) {
				throw new GeneratorException(e.getMessage());
			}	
		}
		
		out.println("");
		out.println("\t\tpublic "+className+"(string "+VALUE+")");
		out.println("\t\t{");
		out.println("\t\t\tthis."+VALUE+" = "+VALUE+";");
		out.println("\t\t}");
	
	    out.println("\t}");
		out.println("}");
	
	} // generate
	
	/************************************************************/
	/* protected methods */
	/************************************************************/
	
	protected void generateConstant(PrintStream out, String name, String value) throws IOException
	{
	String className = m_class.getSimpleName();
			
		out.println("\t\tpublic static "+className+" "+name+" = new "+className+"(\""+value+"\");");
	}
	
} // CSharpEnumGenerator

/************************************************************/


