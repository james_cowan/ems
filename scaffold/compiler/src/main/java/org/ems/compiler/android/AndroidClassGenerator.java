/************************************************************/
/*
	File: AndroidClassGenerator.java

	AndroidClassGenerator generates as Android parcel class from a java class.

	Author: James Cowan
*/
/************************************************************/

package org.ems.compiler.android;

import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.ems.compiler.model.JavaClass;
import org.ems.compiler.model.JavaClassList;
import org.ems.compiler.model.TypeDescriptorList;
import org.ems.generator.GeneratorException;

/************************************************************/

public class AndroidClassGenerator extends AndroidGenerator
{	
	public AndroidClassGenerator(JavaClassList javaClassList, JavaClass javaClass, Class<?> cl, TypeDescriptorList typeDescriptorList)
	throws GeneratorException
	{
		super(javaClassList, javaClass, cl, typeDescriptorList);
	}

	/************************************************************/
	/* public methods */
	/************************************************************/
	/** generate */

	public void generate(PrintStream out) throws IOException, GeneratorException
	{
	List<PropertyDescriptor> propertyDescriptorList = getPropertyDescriptorList();
	HashMap<String, String> importTable = new HashMap<String, String>();
	ArrayList<String> importList = new ArrayList<String>();
	String extension = "";
	String className = m_class.getSimpleName();
	String instanceName = getInstanceName(className);
	String packageName = m_class.getPackage().getName();
	String superArg = m_javaClass.getSuperArg();

		out.println("package "+packageName+";");
		out.println("");

		for (int count=0; count<propertyDescriptorList.size(); count++)
		{
		PropertyDescriptor propertyDescriptor = propertyDescriptorList.get(count);
		String propertyImport = getPropertyImport(propertyDescriptor);
		Class<?> cl = propertyDescriptor.getPropertyType();
		Class<?> parameterisedClass = getParameterisedClass(propertyDescriptor);
		
			if (!packageName.equals(cl.getPackage().getName()))
			{
				if (cl == List.class)
				{
					addImport("import java.util.ArrayList", importTable, importList);
				}
				addImport(propertyImport, importTable, importList);
			}
			if (parameterisedClass != null && !packageName.equals(parameterisedClass.getPackage().getName()))
			{
				addImport(getPropertyImport(parameterisedClass), importTable, importList);
			}
		}
			
		if (hasSuperClass())
		{
		Class<?> superClass = m_class.getSuperclass();
		String superClassImport = "import "+superClass.getName();
		
			extension = " extends "+superClass.getSimpleName();
			
			if (!packageName.equals(superClass.getPackage().getName())
			    && !importList.contains(superClassImport))
			{
				importList.add(superClassImport);
			}
		}
		
		importList.add("import android.os.Parcel");
		importList.add("import android.os.Parcelable");
		
		for (int count=0; count<importList.size(); count++)
		{
			out.println(importList.get(count)+";");
		}

		out.println("");
		out.println("public class "+className+extension+" implements Parcelable");
		out.println("{");
		
		for (PropertyDescriptor propertyDescriptor : propertyDescriptorList)
		{
		String name = propertyDescriptor.getName();
		String typeName = getParmeterisedTypeName(propertyDescriptor);
		
			out.println("\tprotected "+typeName+"\t"+name+";");
		}
		
		// empty constructor
		
		out.println("");
		out.println("\tpublic "+className+"()");
		out.println("\t{");
		if (!superArg.equals(""))
		{
			out.println("\t\tsuper("+superArg+");");
		}
		for (int count=0; count<propertyDescriptorList.size(); count++)
		{
		PropertyDescriptor propertyDescriptor = propertyDescriptorList.get(count);
		Class<?> cl = propertyDescriptor.getPropertyType();
		Class<?> parameterisedClass = getParameterisedClass(propertyDescriptor);
		
			if (cl == List.class && parameterisedClass != null)
			{
			String name = propertyDescriptor.getName();
			String typeName = parameterisedClass.getSimpleName();
				
				out.println("\t\tthis."+name+ " = new ArrayList<"+typeName+">();");	
			}	
		}
			
		out.println("\t}");
		
		// constructor passing instance
		
		if (propertyDescriptorList.size() > 0)
		{
			out.println("");
			out.print("\tpublic "+className+"("+className+" "+instanceName+")");
		
			out.println("\t{");
			generateSuper(out, false);
		
			for (PropertyDescriptor propertyDescriptor : propertyDescriptorList)
			{
			String name = propertyDescriptor.getName();
			String typeName = getPropertyName(propertyDescriptor);
			String functionName = "get"+typeName;
			
				out.println("\t\tthis."+name+ " = "+instanceName+"."+functionName+"();");
			}
			out.println("\t}");
		}
		
		// constructor passing individual arguments
		
		if (propertyDescriptorList.size() > 0 || (hasSuperClass() && superArg.equals("")))
		{
			out.println("");
			out.print("\tpublic "+className+"(");
		
			if (hasSuperClass() && superArg.equals(""))
			{
			String superClassName = m_class.getSuperclass().getSimpleName();
			String superInstanceName = getInstanceName(superClassName);
				
				out.print(superClassName+" "+superInstanceName);
				
				if (propertyDescriptorList.size() > 0)
				{
					out.print(",");
				}
			}
		
			for (int count=0; count<propertyDescriptorList.size(); count++)
			{
			PropertyDescriptor propertyDescriptor = propertyDescriptorList.get(count);
			String name = propertyDescriptor.getName();
			String typeName = getParmeterisedTypeName(propertyDescriptor);
		
				if (count > 0)
				{
					out.print(",");
				}
				out.print(typeName+" "+name);
			}
			out.println(")");
			out.println("\t{");
			generateSuper(out, true);
		
			for (PropertyDescriptor propertyDescriptor : propertyDescriptorList)
			{
			String name = propertyDescriptor.getName();
			
				out.println("\t\tthis."+name+ " = "+name+";");
			}
			out.println("\t}");
		}
		
		for (PropertyDescriptor propertyDescriptor : propertyDescriptorList)
		{
		String name = getPropertyName(propertyDescriptor);
		String propertyName = propertyDescriptor.getName();
		String functionName = "get"+name;
		String typeName = getParmeterisedTypeName(propertyDescriptor);
		
			out.println("");
			out.println("\tpublic "+typeName+" "+functionName+"()");
			out.println("\t{");
			out.println("\t\treturn this."+propertyName+";");
			out.println("\t}");
		}
		
		for (PropertyDescriptor propertyDescriptor : propertyDescriptorList)
		{
		String name = getPropertyName(propertyDescriptor);
		String propertyName = propertyDescriptor.getName();
		String functionName = "set"+name;
		String typeName = getParmeterisedTypeName(propertyDescriptor);
		
			out.println("");
			out.println("\tpublic void "+functionName+"("+typeName+" "+propertyName+")");
			out.println("\t{");
			out.println("\t\tthis."+propertyName+" = "+propertyName+";");
			out.println("\t}");
		}
		
		generateParcelMethods(out);

		out.println("");
		out.println("} // "+className);		

	} // generate
	
	/************************************************************/
	/* protected methods */
	/************************************************************/
	
	private void generateSuper(PrintStream out, boolean parent) throws IOException
	{
	String superArg = m_javaClass.getSuperArg();
		
		if (!superArg.equals(""))
		{
			out.println("\t\tsuper("+superArg+");");
		}
		else
		{
			if (hasSuperClass())
			{
			Class<?> cl = (parent) ? m_class.getSuperclass() : m_class;
			String className = cl.getSimpleName();
			String instanceName = getInstanceName(className);
			
				out.println("\t\tsuper("+instanceName+");");
			}
		}
		
	}

} // AndroidClassGenerator

/************************************************************/


