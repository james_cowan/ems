/************************************************************/
/*
	File: TypeScriptGenerator.java

	TypeScriptGenerator generates as TypeScript class file from a java class.

	Author: James Cowan
*/
/************************************************************/

package org.ems.compiler.typescript;

import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ems.compiler.ClassGenerator;
import org.ems.compiler.JavaCompiler;
import org.ems.compiler.model.ImportDescriptor;
import org.ems.compiler.model.JavaClass;
import org.ems.compiler.model.JavaClassList;
import org.ems.compiler.model.TypeDescriptor;
import org.ems.compiler.model.TypeDescriptorList;
import org.ems.generator.GeneratorException;

/************************************************************/

public class TypeScriptGenerator extends ClassGenerator {
	protected JavaClassList m_javaClassList;
	protected Map<String, JavaClass> m_javaClassMap;

	public TypeScriptGenerator(JavaClassList javaClassList, JavaClass javaClass, Class<?> cl,
			TypeDescriptorList typeDescriptorList) throws GeneratorException {
		super(javaClass, cl, typeDescriptorList);

		m_javaClassList = javaClassList;
		m_javaClassMap = getJavaClassMap(javaClassList);
	}

	/************************************************************/
	/* public methods */
	/************************************************************/
	/** generate */

	public void generate(PrintStream out) throws IOException, GeneratorException {

	} // generate

	/************************************************************/
	/* protected methods */
	/************************************************************/
	/** addImport */

	protected void addImport(ImportDescriptor importDescriptor, Map<String, ImportDescriptor> importTable,
			List<ImportDescriptor> importList) {
		String importName = importDescriptor.getTypeName();

		if (importTable.get(importName) == null) {
			importTable.put(importName, importDescriptor);
			importList.add(importDescriptor);
		}
	}

	/************************************************************/
	/**
	 * getJavaClassMap
	 * 
	 * @throws GeneratorException
	 */

	protected Map<String, JavaClass> getJavaClassMap(JavaClassList javaClassList) throws GeneratorException {
		Map<String, JavaClass> result = new HashMap<>();

		for (JavaClass javaClass : javaClassList.getJavaClass()) {
			Class<?> cl = JavaCompiler.getJavaClass(javaClass.getClassName());

			result.put(cl.getSimpleName(), javaClass);
		}
		return result;
	}

	/************************************************************/
	/** getPropertyType */

	protected PropertyType getPropertyType(PropertyDescriptor propertyDescriptor) {
		Class<?> cl = propertyDescriptor.getPropertyType();

		if (cl.isArray()) {
			Class<?> componentType = cl.getComponentType();

			return getParameterisedPropertyType(componentType);
		}

		if (cl == List.class) {
			Type type = propertyDescriptor.getReadMethod().getGenericReturnType();
			ParameterizedType pType = (ParameterizedType) type;
			Class<?> pClass = (Class<?>) pType.getActualTypeArguments()[0];

			return getParameterisedPropertyType(pClass);
		}

		String moduleName = "";
		String typeName = propertyDescriptor.getPropertyType().getSimpleName();

		if (m_typeDescriptorMap.get(typeName) == null) {
			if (!hasInterface(typeName)) {
				moduleName = typeName;

			} else {
				moduleName = typeName = ClassGenerator.INTERFACE + typeName;
			}
		} else {
			typeName = super.getPropertyTypeName(propertyDescriptor);
		}

		return new PropertyType(moduleName, typeName);

	} // getPropertyType
	
	/************************************************************/
	/** getParameterisedPropertyType */

	protected PropertyType getParameterisedPropertyType(Class<?> cl) {
		final String typeName = cl.getSimpleName();
		TypeDescriptor typeDescriptor = m_typeDescriptorMap.get(typeName);

		if (typeDescriptor != null) {
			return new PropertyType(typeDescriptor.getImport(), "Array<" + typeDescriptor.getTargetName() + ">");
		}

		if (hasInterface(typeName)) {
			final String ifName = ClassGenerator.INTERFACE + typeName;
			
			return new PropertyType(ifName, "Array<" + ifName + ">");
		}

		return new PropertyType(typeName, "Array<" + typeName + ">");
	}
	
	/************************************************************/
	/** hasInterface */
	
	protected boolean hasInterface(String typeName) {
		JavaClass javaClass = m_javaClassMap.get(typeName);

		if (javaClass != null && javaClass.getGenerateInterface()) {
			return true;
		}
		
		return false;
	}
	
	/************************************************************/

	class PropertyType {
		String moduleName;
		String typeName;

		public PropertyType(String moduleName, String typeName) {
			this.moduleName = moduleName;
			this.typeName = typeName;
		}

	}

} // TypeScriptGenerator

/************************************************************/
