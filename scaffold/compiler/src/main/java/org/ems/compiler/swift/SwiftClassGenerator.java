/************************************************************/
/*
	File: SwiftClassGenerator.java

	Author: James Cowan
*/
/************************************************************/

package org.ems.compiler.swift;

import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ems.compiler.model.JavaClass;
import org.ems.compiler.model.JavaClassList;
import org.ems.compiler.model.TypeDescriptorList;
import org.ems.generator.GeneratorException;

/************************************************************/

public class SwiftClassGenerator extends SwiftGenerator
{	
	public SwiftClassGenerator(JavaClassList javaClassList, JavaClass javaClass, Class<?> cl, TypeDescriptorList typeDescriptorList)
	throws GeneratorException
	{
		super(javaClassList, javaClass, cl, typeDescriptorList);
	}

	/************************************************************/
	/* public methods */
	/************************************************************/
	/** generate */

	public void generate(PrintStream out) throws IOException, GeneratorException
	{
	List<PropertyDescriptor> propertyDescriptorList = getPropertyDescriptorList();
	List<PropertyDescriptor> parentPropertyDescriptorList = new ArrayList<PropertyDescriptor>();
	HashMap<String, String> importTable = new HashMap<String, String>();
	ArrayList<String> importList = new ArrayList<String>();
	String extension = "";
	String className = m_class.getSimpleName();
	String instanceName = getInstanceName(className);
	String packageName = m_class.getPackage().getName();
	String superArg = m_javaClass.getSuperArg();

		for (int count=0; count<propertyDescriptorList.size(); count++)
		{
		PropertyDescriptor propertyDescriptor = propertyDescriptorList.get(count);
		String propertyImport = getPropertyImport(propertyDescriptor);
		Class<?> cl = propertyDescriptor.getPropertyType();
		Class<?> parameterisedClass = getParameterisedClass(propertyDescriptor);
		
			if (!packageName.equals(cl.getPackage().getName()))
			{
				addImport(propertyImport, importTable, importList);
			}
			if (parameterisedClass != null && !packageName.equals(parameterisedClass.getPackage().getName()))
			{
				addImport(getPropertyImport(parameterisedClass), importTable, importList);
			}
		}
			
		if (hasSuperClass())
		{
		Class<?> superClass = m_class.getSuperclass();
		JavaClass superJavaClass = getJavaClass(superClass);
		Map<String, String> excludedPropertyMap = createExcludedPropertyMap(superJavaClass);
		
			extension = " : "+superClass.getSimpleName();
			parentPropertyDescriptorList = getPropertyDescriptorList(superJavaClass, excludedPropertyMap);
		}

		out.println("");
		out.println("public class "+className+extension+" {");
		
		for (PropertyDescriptor propertyDescriptor : propertyDescriptorList)
		{
		String name = propertyDescriptor.getName();
		String typeName = getParmeterisedTypeName(propertyDescriptor);
		
			out.println("\tvar \t"+name+":"+typeName);
		}
		
		// empty constructor
		
		/*
		out.println("");

		out.println("\tinit() {");
		if (!superArg.equals(""))
		{
			out.println("\t\tsuper.init("+superArg+")");
		}
		out.println("\t}");
		*/
		
		// constructor passing individual arguments
		
		if (propertyDescriptorList.size() > 0 || (hasSuperClass() && superArg.equals("")))
		{
			out.println("");
			out.print("\tinit(");
		
			if (hasSuperClass() && superArg.equals(""))
			{
			String superClassName = m_class.getSuperclass().getSimpleName();
			String superInstanceName = getInstanceName(superClassName);
				
				out.print(superInstanceName+":"+superClassName);
				
				if (propertyDescriptorList.size() > 0)
				{
					out.print(",");
				}
			}
		
			for (int count=0; count<propertyDescriptorList.size(); count++)
			{
			PropertyDescriptor propertyDescriptor = propertyDescriptorList.get(count);
			String name = propertyDescriptor.getName();
			String typeName = getParmeterisedTypeName(propertyDescriptor);
		
				if (count > 0)
				{
					out.print(",");
				}
				out.print(name+":"+typeName);
			}
			out.println(") {");
			out.println("");
		
			for (PropertyDescriptor propertyDescriptor : propertyDescriptorList)
			{
			String name = propertyDescriptor.getName();
			
				out.println("\t\tself."+name+ " = "+name);
			}
			
			if (parentPropertyDescriptorList.size() > 0 && superArg.equals(""))
			{
			int propertyCount = 0;	
			Class<?> superClass = m_class.getSuperclass();
			String superInstanceName = getInstanceName(superClass.getSimpleName());
			List<String> superClassList = new ArrayList<String>();
			
				getSuperClass(superClass.getSuperclass(), superClassList);
				
				out.print("\t\tsuper.init(");
				
				for (String superClassName : superClassList)
				{
					if (propertyCount++ > 0)
					{
						out.print(",");
					}
				
					out.print(getInstanceName(superClassName)+":"+superInstanceName);
				}
				
				for (PropertyDescriptor parentProperty : parentPropertyDescriptorList)
				{
				String propertyName = parentProperty.getName();
				
					if (propertyCount++ > 0)
					{
						out.print(",");
					}
				
					out.print(propertyName+":"+superInstanceName+"."+propertyName);	
				}
				out.println(")");
			}
			
			if (!superArg.equals("")) {
				out.println("\t\tsuper.init("+superArg+")");
			}
			
			out.println("\t}");
		}
		
		// constructor passing instance
		
		if (propertyDescriptorList.size() > 0)
		{
		int propertyCount = 0;
		
			out.println("");
			out.println("\tconvenience init("+instanceName+":"+className+") {");
			
			out.print("\t\tself.init(");
			
			if (hasSuperClass() && superArg.equals(""))
			{
			String superClassName = m_class.getSuperclass().getSimpleName();
			String superInstanceName = getInstanceName(superClassName);
			
				out.print(superInstanceName+":"+instanceName);
				propertyCount++;
			}
		
			for (PropertyDescriptor propertyDescriptor : propertyDescriptorList)
			{
			String propertyName = propertyDescriptor.getName();
			
				if (propertyCount++ > 0)
				{
					out.print(",");
				}
			
				out.print(propertyName+":"+instanceName+"."+propertyName);	
			}
			
			out.println(")");
			
			out.println("\t}");
		}
/*		
		for (PropertyDescriptor propertyDescriptor : propertyDescriptorList)
		{
		String name = getPropertyName(propertyDescriptor);
		String propertyName = propertyDescriptor.getName();
		String functionName = "get"+name;
		String typeName = getParmeterisedTypeName(propertyDescriptor);
		
			out.println("");
			out.println("\tpublic "+typeName+" "+functionName+"()");
			out.println("\t{");
			out.println("\t\treturn this."+propertyName+";");
			out.println("\t}");
		}
		
		for (PropertyDescriptor propertyDescriptor : propertyDescriptorList)
		{
		String name = getPropertyName(propertyDescriptor);
		String propertyName = propertyDescriptor.getName();
		String functionName = "set"+name;
		String typeName = getParmeterisedTypeName(propertyDescriptor);
		
			out.println("");
			out.println("\tpublic void "+functionName+"("+typeName+" "+propertyName+")");
			out.println("\t{");
			out.println("\t\tthis."+propertyName+" = "+propertyName+";");
			out.println("\t}");
		}
		
*/
		out.println("");
		out.println("} // "+className);		

	} // generate
	
	/************************************************************/
	/* protected methods */
	/************************************************************/

} // SwiftClassGenerator

/************************************************************/


