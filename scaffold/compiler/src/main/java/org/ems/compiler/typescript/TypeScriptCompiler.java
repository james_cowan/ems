/************************************************************/
/*
	File: TypeScriptCompiler.java

	JavaToTypeScriptCompiler compiles Java beans to TypeScript beans.

	Author: James Cowan
*/
/************************************************************/

package org.ems.compiler.typescript;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;

import org.ems.compiler.ClassGenerator;
import org.ems.compiler.JavaCompiler;
import org.ems.compiler.model.ImportDescriptor;
import org.ems.compiler.model.JavaClass;
import org.ems.compiler.model.TypeDescriptor;
import org.ems.compiler.model.TypeDescriptorList;
import org.ems.generator.Generator;
import org.ems.generator.GeneratorException;

/************************************************************/

public class TypeScriptCompiler extends JavaCompiler
{
	protected TypeDescriptorList	m_typeDescriptorList;
	
	public TypeScriptCompiler(File schemaDirectory, File schemaFile, File output)
	throws GeneratorException
	{
		super(schemaDirectory, schemaFile, output);
		
		m_typeDescriptorList = getTypeDescriptorList();
	}

	/************************************************************/
	/* public methods */
	/************************************************************/

	
	/************************************************************/
	/* protected methods */
	/************************************************************/
	/** compileClassList */
	
	protected void compileClassList() throws IOException, GeneratorException
	{
		if (m_javaClassList.getGroupClasses())
		{
			compileModule();
			return;
		}
	
		JavaClass javaClasses[] = m_javaClassList.getJavaClass();
	
		for (int count=0; count<javaClasses.length; count++)
		{
		JavaClass javaClass = javaClasses[count];
		Class<?> cl = getJavaClass(javaClass);
		File dir = Generator.createPackageDir(m_output, getOutputDirectory(cl));
		File classFile = new File(dir, cl.getSimpleName()+".ts");
		File ifFile = new File(dir, "I"+cl.getSimpleName()+".ts");
		
			if (javaClass.getGenerateInterface())
			{	
			TypeScriptInterfaceGenerator ifGenerator = new TypeScriptInterfaceGenerator(m_javaClassList, javaClass, cl, m_typeDescriptorList);
				
				generate(ifFile, ifGenerator);
			}
			
			if (cl.isEnum()) 
			{
			TypeScriptEnumGenerator enumGenerator = new TypeScriptEnumGenerator(m_javaClassList, javaClass, cl, m_typeDescriptorList);
				
				generate(classFile, enumGenerator);
			}
			else
			{
			TypeScriptClassGenerator clGenerator = new TypeScriptClassGenerator(m_javaClassList, javaClass, cl, m_typeDescriptorList);
				
				generate(classFile, clGenerator);
			}
		}
		
	} // compileClassList
	
	/************************************************************/
	/** compileModule */
	
	protected void compileModule() throws IOException, GeneratorException
	{
	JavaClass javaClasses[] = m_javaClassList.getJavaClass();
	File dir = Generator.createPackageDir(m_output, m_javaClassList.getModuleDirectory());
	File file = new File(dir, m_javaClassList.getModuleName());
	FileOutputStream fos = new FileOutputStream(file);
	PrintStream ps = new PrintStream(fos);
			
		m_log.info("Create module: "+file.getAbsolutePath());
		
		ImportDescriptor classImports[] = m_javaClassList.getImportDescriptor();
		
		if (classImports.length > 0)
		{
			for (ImportDescriptor classImport : classImports)
			{
				ps.println("import "+classImport.getTypeName()+" from \""+classImport.getPath()+"\";");
			}
			ps.println("");
		}
		
		for (int count=0; count<javaClasses.length; count++)
		{
		JavaClass javaClass = javaClasses[count];
		Class<?> cl = getJavaClass(javaClass);
		TypeScriptInterfaceGenerator ifGenerator = new TypeScriptInterfaceGenerator(m_javaClassList, javaClass, cl, m_typeDescriptorList);
		TypeScriptClassGenerator clGenerator = new TypeScriptClassGenerator(m_javaClassList, javaClass, cl, m_typeDescriptorList);
		TypeScriptEnumGenerator enumGenerator = new TypeScriptEnumGenerator(m_javaClassList, javaClass, cl, m_typeDescriptorList);
		
			if (javaClass.getGenerateInterface())
			{
				ifGenerator.generate(ps);
			}
		
			if (cl.isEnum()) 
			{
				enumGenerator.generate(ps);
			}
			else
			{
				clGenerator.generate(ps);
			}
		}
		
		ps.close();
		
	} // compileModule
	
	/************************************************************/
	/** generate */
	
	protected void generate(File file, ClassGenerator generator) throws IOException, GeneratorException
	{	
	FileOutputStream fos = new FileOutputStream(file);
	PrintStream ps = new PrintStream(fos);
			
		m_log.info("Create ts class: "+file.getAbsolutePath());
		
		generator.generate(ps);
		ps.close();
	}
	
	/************************************************************/
	/** getOutputDirectory */
	
	protected String getOutputDirectory(Class<?> cl)
	{
		if (m_javaClassList.getGeneratePackageDirectory())
		{
			return cl.getPackage().getName();
		}
		
		return m_javaClassList.getModuleDirectory();
	}
	
	/************************************************************/
	/** getTypeDescriptorList */
	
	protected TypeDescriptorList getTypeDescriptorList()throws GeneratorException
	{	
	String fileName = "/tstypes.xml";
	
        try
        {
        InputStream in = getClass().getResourceAsStream(fileName);
        InputStreamReader reader = new InputStreamReader(in);
		TypeDescriptorList typeDescriptorList = TypeDescriptorList.unmarshalTypeDescriptorList(reader);
		TypeDescriptor classTypeDescriptors[] = m_javaClassList.getTypeDescriptor();
		
			for (TypeDescriptor classTypeDescriptor : classTypeDescriptors)
			{
				typeDescriptorList.addTypeDescriptor(classTypeDescriptor);
			}
		
			return typeDescriptorList;
        }
        catch (Throwable e)
        {
        String msg = "Cannot read "+fileName+": "+e.getMessage();
        	
        	throw new GeneratorException(msg);
        }
        	
	} // getTypeDescriptorList
	
} // TypeScriptCompiler

/************************************************************/


