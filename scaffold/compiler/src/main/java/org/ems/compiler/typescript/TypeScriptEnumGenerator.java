/************************************************************/
/*
	File: TypeScriptInterfaceGenerator.java

	TypeScriptInterfaceGenerator generates as TypeScript class file from a java class.

	Author: James Cowan
*/
/************************************************************/

package org.ems.compiler.typescript;

import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import org.ems.compiler.model.JavaClass;
import org.ems.compiler.model.JavaClassList;
import org.ems.compiler.model.TypeDescriptorList;
import org.ems.generator.GeneratorException;

/************************************************************/

public class TypeScriptEnumGenerator extends TypeScriptGenerator
{	
	public TypeScriptEnumGenerator(JavaClassList javaClassList, JavaClass javaClass, Class<?> cl, TypeDescriptorList typeDescriptorList)
	throws GeneratorException
	{
		super(javaClassList, javaClass, cl, typeDescriptorList);
	}

	/************************************************************/
	/* public methods */
	/************************************************************/
	/** generate */

	public void generate(PrintStream out) throws IOException, GeneratorException
	{
	String className = m_class.getSimpleName();
	List<?> enumConstants = Arrays.asList(m_class.getEnumConstants());
	String enumValueName = m_javaClass.getEnumValue();
	Class<? extends Enum> enumClass = (Class<? extends Enum>) m_class;

		out.println("export default class "+className+ " {");
		
		if (enumValueName.equals("")) {
			for (Object enumConstant : enumConstants) {
				out.println("\tpublic static "+enumConstant+":string=\""+enumConstant+"\";");
			}
		}
		else {
			try {
				for (Object enumConstant : enumConstants) {
					final Enum<?> enumValue = Enum.valueOf(enumClass, enumConstant.toString());
					Field f = m_class.getDeclaredField(enumValueName);
						
					f.setAccessible(true);	
					out.println("\tpublic static "+enumConstant+":string=\""+f.get(enumValue)+"\";");
				}
				
			} catch (Exception e) {
				throw new GeneratorException(e.getMessage());
			}	
		}
		
		out.println("}");
		
		if (m_javaClassList.getGroupClasses())
		{
			out.println("");
		}
	
	} // generate
	
	/************************************************************/
	/* protected methods */
	/************************************************************/
	

} // TypeScriptInterfaceGenerator

/************************************************************/


