/************************************************************/
/*
	File: CSharpClassGenerator.java
	
	Author: James Cowan
*/
/************************************************************/

package org.ems.compiler.csharp;

import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.ems.compiler.model.JavaClass;
import org.ems.compiler.model.JavaClassList;
import org.ems.compiler.model.TypeDescriptorList;
import org.ems.generator.GeneratorException;

/************************************************************/

public class CSharpClassGenerator extends CSharpGenerator
{	
	public CSharpClassGenerator(JavaClassList javaClassList, JavaClass javaClass, Class<?> cl, TypeDescriptorList typeDescriptorList)
	throws GeneratorException
	{
		super(javaClassList, javaClass, cl, typeDescriptorList);
	}

	/************************************************************/
	/* public methods */
	/************************************************************/
	/** generate */

	public void generate(PrintStream out) throws IOException, GeneratorException
	{
	List<PropertyDescriptor> propertyDescriptorList = getPropertyDescriptorList();	
	String extension = "";
	String className = m_class.getSimpleName();
	String instanceName = getInstanceName(className);
	String nameSpace = m_javaClassList.getNameSpace();
	String superArg = m_javaClass.getSuperArg();
	List<String> importList = new ArrayList<String>();
	
		for (PropertyDescriptor propertyDescriptor : propertyDescriptorList)
		{
		String importPath = getParmeterisedImport(propertyDescriptor);
	
			if (!importPath.equals(""))
			{
				importList.add(importPath);
			}
		}
		
		addImport(importList);
	
		for (String importPath : importList)
		{
			out.println("using "+importPath+";");
		}
		
		if (importList.size() > 0)
		{
			out.println("");
		}

		out.println("namespace "+nameSpace);
		out.println("{");
			
		if (hasSuperClass())
		{
		Class<?> superClass = m_class.getSuperclass();
		
			extension = " : "+superClass.getSimpleName();
		}

		out.println("");
		out.println("\tpublic class "+className+extension);
		out.println("\t{");
		
		for (PropertyDescriptor propertyDescriptor : propertyDescriptorList)
		{
		String name = propertyDescriptor.getName();
		String typeName = getParmeterisedTypeName(propertyDescriptor);
		
			out.println("\t\tpublic "+typeName+"\t"+name+" { get; set; }");
		}
		
		// empty constructor
		
		out.println("");
		out.print("\t\tpublic "+className+"()");
		if (!superArg.equals(""))
		{
			out.println(" : base("+superArg+")");
		}
		else
		{
			out.println("");
		}
		out.println("\t\t{");
		out.println("\t\t}");
		
		// constructor passing instance
		
		if (propertyDescriptorList.size() > 0)
		{
			out.println("");
			out.print("\t\tpublic "+className+"("+className+" "+instanceName+")");
		
			generateSuper(out, false);

			out.println("\t\t{");
			
			for (PropertyDescriptor propertyDescriptor : propertyDescriptorList)
			{
			String name = propertyDescriptor.getName();
			
				out.println("\t\t\tthis."+name+ " = "+instanceName+"."+name+";");
			}
			out.println("\t\t}");
		}
		
		// constructor passing individual arguments
		
		if (propertyDescriptorList.size() > 0 || (hasSuperClass() && superArg.equals("")))
		{
			out.println("");
			out.print("\t\tpublic "+className+"(");
		
			if (hasSuperClass() && superArg.equals(""))
			{
			String superClassName = m_class.getSuperclass().getSimpleName();
			String superInstanceName = getInstanceName(superClassName);
				
				out.print(superClassName+" "+superInstanceName);
				
				if (propertyDescriptorList.size() > 0)
				{
					out.print(",");
				}
			}
		
			for (int count=0; count<propertyDescriptorList.size(); count++)
			{
			PropertyDescriptor propertyDescriptor = propertyDescriptorList.get(count);
			String name = propertyDescriptor.getName();
			String typeName = getParmeterisedTypeName(propertyDescriptor);
		
				if (count > 0)
				{
					out.print(",");
				}
				out.print(typeName+" "+name);
			}
			out.print(")");
			generateSuper(out, true);
			out.println("\t\t{");
			
			for (PropertyDescriptor propertyDescriptor : propertyDescriptorList)
			{
			String name = propertyDescriptor.getName();
			
				out.println("\t\t\tthis."+name+ " = "+name+";");
			}
			out.println("\t\t}");
		}

		out.println("");
		out.println("\t} // "+className);
		out.println("}");

	} // generate
	
	/************************************************************/
	/* protected methods */
	/************************************************************/
	
	private void generateSuper(PrintStream out, boolean parent) throws IOException
	{
	String superArg = m_javaClass.getSuperArg();
		
		if (!superArg.equals(""))
		{
			out.println(" : base("+superArg+")");
		}
		else
		{
			if (hasSuperClass())
			{
			Class<?> cl = (parent) ? m_class.getSuperclass() : m_class;
			String className = cl.getSimpleName();
			String instanceName = getInstanceName(className);
			
				out.println(": base("+instanceName+")");
			}
		}
		
	}

} // CSharpClassGenerator

/************************************************************/


