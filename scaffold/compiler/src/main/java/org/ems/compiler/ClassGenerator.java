/************************************************************/
/*
	File: ClassGenerator.java

	ClassGenerator converts a java class into (e.g) an AS3 class.

	Author: James Cowan
*/
/************************************************************/

package org.ems.compiler;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ems.compiler.model.JavaClass;
import org.ems.compiler.model.TypeDescriptor;
import org.ems.compiler.model.TypeDescriptorList;
import org.ems.generator.ClassMetaData;
import org.ems.generator.GeneratorException;

/************************************************************/

abstract public class ClassGenerator 
{
	protected JavaClass							m_javaClass;
	protected Class<?>							m_class;
	protected Map<String, String>				m_excludedPropertyMap;
	protected Map<String, TypeDescriptor>		m_typeDescriptorMap;
	
	public static final String	INTERFACE = "I";
	
	public ClassGenerator(JavaClass javaClass, Class<?> cl, TypeDescriptorList typeDescriptorList)
	throws GeneratorException
	{
	TypeDescriptor typeDescriptors[] = typeDescriptorList.getTypeDescriptor();
	
		m_javaClass = javaClass;
		m_class = cl;
		m_excludedPropertyMap = createExcludedPropertyMap(javaClass);
		m_typeDescriptorMap = new HashMap<String, TypeDescriptor>();		
		
		for (int count=0; count<typeDescriptors.length; count++)
		{	
		TypeDescriptor typeDescriptor = typeDescriptors[count];
			
			m_typeDescriptorMap.put(typeDescriptor.getName(), typeDescriptor);
		}
	}

	/************************************************************/
	/* public methods */
	/************************************************************/

	abstract public void generate(PrintStream out) throws IOException, GeneratorException;
	
	/************************************************************/
	/* protected methods */
	/************************************************************/
	/** createExcludedPropertyMap */
	
	protected Map<String, String> createExcludedPropertyMap(JavaClass javaClass)
	{
	Map<String, String> excludedPropertyMap = new HashMap<String, String>();
	String excludedProperties[] = javaClass.getExcludedProperty();
	
		for (int count=0; count<excludedProperties.length; count++)
		{	
		String name = excludedProperties[count];

			m_excludedPropertyMap.put(name, name);
		}
	
		return excludedPropertyMap;
	}
	/************************************************************/
	/** getInstanceName */
	
	protected String getInstanceName(String className)
	{
		if (className.equals(""))
		{
			return className;
		}
		
		String first = className.substring(0, 1);
		
		return first.toLowerCase()+className.substring(1);
		
	} // getInstanceName
	
	/************************************************************/
	/** getParameterisedClass */
	
	protected Class<?> getParameterisedClass(PropertyDescriptor descriptor) 
	{
	Class<?> cl = descriptor.getPropertyType();
		
		if (cl.isArray())
		{
		Class<?> componentType = cl.getComponentType();
		
			return componentType;
		}
		
		if (cl == List.class)
		{
		Type type = descriptor.getReadMethod().getGenericReturnType();
		ParameterizedType pType = (ParameterizedType) type;
		Class<?> pClass = (Class<?>) pType.getActualTypeArguments()[0];
		
			return pClass;
		}
		
		return null;
	}
	
	/************************************************************/
	/** getPropertyDescriptorList */
	
	protected List<PropertyDescriptor> getPropertyDescriptorList() throws GeneratorException
	{
		return getPropertyDescriptorList(m_javaClass, m_excludedPropertyMap);
	}
	
	/************************************************************/
	/** getPropertyDescriptorList */
	
	protected List<PropertyDescriptor> getPropertyDescriptorList(JavaClass javaClass, Map<String, String> excludedPropertyMap) throws GeneratorException
	{
	String className = javaClass.getClassName();
	
		try
		{
		ClassMetaData md = new ClassMetaData(className, "");
		PropertyDescriptor descriptors[] = md.getPropertyDescriptors();
		ArrayList<PropertyDescriptor> resultList = new ArrayList<PropertyDescriptor>();
	
			for (int count=0; count<descriptors.length; count++)
			{
			PropertyDescriptor propertyDescriptor = descriptors[count];
			String name = propertyDescriptor.getName();
			
				if (excludedPropertyMap.get(name) != null)
				{
					continue;
				}
			
				resultList.add(propertyDescriptor);
			}
			return resultList;
		}
		catch (ClassNotFoundException cne)
		{
			throw new GeneratorException("Invalid class: "+className);
		}
		catch (IntrospectionException ie)
		{
			throw new GeneratorException(ie.getMessage());
		}
		
	} // getPropertyDescriptorList
	
	/************************************************************/
	/** getPropertyImport */
	
	protected String getPropertyImport(PropertyDescriptor propertyDescriptor)
	{
	Class<?> cl = propertyDescriptor.getPropertyType();
	String typeName = cl.getSimpleName();
	TypeDescriptor typeDescriptor = m_typeDescriptorMap.get(typeName);
	
		if (typeDescriptor != null)
		{
			return typeDescriptor.getImport();
		}
		
		Package pkg = cl.getPackage();
		
		if (pkg == null)
		{
			return "";
		}
		
		//System.out.println("name "+propertyDescriptor.getName()+" class "+cl.getName()+" pkg "+pkg);
		
		return getPropertyImport(cl);
		
	} // getPropertyImport
	
	/************************************************************/
	/** getPropertyImport */
	
	protected String getPropertyImport(Class<?> cl)
	{
		return "import "+cl.getPackage().getName()+"."+cl.getSimpleName();	
	}
	
	/************************************************************/
	/** getPropertyName */
	
	protected String getPropertyName(PropertyDescriptor propertyDescriptor)
	{
	String name = propertyDescriptor.getName();
	
		if (name.equals(""))
		{
			return "";
		}
		
		String firstChar = name.substring(0, 1).toUpperCase();
		
		String remainder = "";
	
		if (name.length() > 1)
		{
			remainder = name.substring(1);
		}
	
		return firstChar.toUpperCase()+remainder;
	
	} // getPropertyTypeName
	
	/************************************************************/
	/** getPropertyTypeName */
	
	protected String getPropertyTypeName(PropertyDescriptor propertyDescriptor)
	{
	String typeName = propertyDescriptor.getPropertyType().getSimpleName();
	
		return getPropertyTypeName(typeName);
	
	} // getPropertyTypeName

	/************************************************************/
	/** getPropertyTypeName */
	
	protected String getPropertyTypeName(String typeName)
	{
	TypeDescriptor typeDescriptor = m_typeDescriptorMap.get(typeName);
	
		if (typeDescriptor != null)
		{
			return typeDescriptor.getTargetName();
		}
		
		return typeName;
		
	} // getPropertyTypeName
	
	/************************************************************/
	/** getSuperClass */
	
	protected void getSuperClass(Class<?>cl, List<String>classList)
	{
		if (cl == Object.class)
		{
			return;
		}
		
		classList.add(cl.getSimpleName());
		
		getSuperClass(cl.getSuperclass(), classList);
		
	} // hasSuperClass
	
	/************************************************************/
	/** hasSuperClass */
	
	protected boolean hasSuperClass(Class<?>cl)
	{
		if (cl.getSuperclass() == Object.class)
		{
			return false;
		}
		if (cl.isEnum())
		{
			return false;
		}
		
		return true;
		
	} // hasSuperClass
	
	/************************************************************/
	/** hasSuperClass */
	
	protected boolean hasSuperClass()
	{
		return hasSuperClass(m_class);
		
	} // hasSuperClass
	
} // ClassGenerator

/************************************************************/


