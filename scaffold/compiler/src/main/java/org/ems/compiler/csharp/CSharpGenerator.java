/************************************************************/
/*
	File: CSharpGenerator.java

	Author: James Cowan
*/
/************************************************************/

package org.ems.compiler.csharp;

import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import org.ems.compiler.ClassGenerator;
import org.ems.compiler.model.ImportDescriptor;
import org.ems.compiler.model.JavaClass;
import org.ems.compiler.model.JavaClassList;
import org.ems.compiler.model.TypeDescriptorList;
import org.ems.generator.GeneratorException;

/************************************************************/

public class CSharpGenerator extends ClassGenerator
{	
	protected JavaClassList m_javaClassList;
	
	public CSharpGenerator(JavaClassList javaClassList, JavaClass javaClass, Class<?> cl, TypeDescriptorList typeDescriptorList)
	throws GeneratorException
	{
		super(javaClass, cl, typeDescriptorList);
		
		m_javaClassList = javaClassList;
	}

	/************************************************************/
	/* public methods */
	/************************************************************/
	/** generate */

	public void generate(PrintStream out) throws IOException, GeneratorException
	{
	
	} // generate
	
	/************************************************************/
	/* protected methods */
	/************************************************************/
	/** addImport */
	
	protected void addImport(List<String> importList)
	{
		addImport(m_javaClassList.getImportDescriptor(), importList);
		addImport(m_javaClass.getImportDescriptor(), importList);
	}
	
	protected void addImport(ImportDescriptor importDescriptorList[], List<String> importList)
	{
		for (ImportDescriptor importDescriptor : importDescriptorList)
		{
		String path = importDescriptor.getPath();
		String typeName = importDescriptor.getTypeName();
		
			if (!typeName.equals(""))
			{
				path += "."+typeName;
			}
			importList.add(path);
		}
	}
	
	protected String getParmeterisedTypeName(PropertyDescriptor descriptor) 
	{
	Class<?> cl = descriptor.getPropertyType();
		
		if (cl.isArray())
		{
		Class<?> componentType = cl.getComponentType();
		String className = componentType.getSimpleName();
		
			return className+"[]";
		}
		
		if (cl == List.class)
		{
		Type type = descriptor.getReadMethod().getGenericReturnType();
		ParameterizedType pType = (ParameterizedType) type;
		Class<?> pClass = (Class<?>) pType.getActualTypeArguments()[0];
		
			return "List<"+pClass.getSimpleName()+">";
		}
		
		return super.getPropertyTypeName(descriptor);
	}
	
	protected String getParmeterisedImport(PropertyDescriptor descriptor) 
	{
	Class<?> cl = descriptor.getPropertyType();
		
		if (cl == List.class)
		{
			return "System.Collections.Generic";
		}
		
		return "";
	}

} // CSharpGenerator

/************************************************************/


