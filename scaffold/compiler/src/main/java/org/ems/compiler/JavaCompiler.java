/************************************************************/
/*
	File: JavaCompiler.java

	JavaCompiler reads a list of java classes to compile and
	generates classes for other languages (as3 etc). 

	Author: James Cowan
*/
/************************************************************/

package org.ems.compiler;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.ems.compiler.model.JavaClass;
import org.ems.compiler.model.JavaClassList;
import org.ems.generator.Generator;
import org.ems.generator.GeneratorException;
import org.ems.generator.IGenerator;
import org.ems.generator.IGeneratorOutput;

/************************************************************/

abstract public class JavaCompiler extends Generator implements IGenerator
{
	protected JavaClassList		m_javaClassList;

	static protected Logger		m_log = Logger.getLogger(JavaCompiler.class);
	
	public JavaCompiler(File schemaDirectory, File schemaFile, File output)
	throws GeneratorException
	{
		super(schemaDirectory, schemaFile, output);
		
		m_javaClassList = getJavaClassList();
	}

	/************************************************************/
	/* public methods */
	/************************************************************/
	/** generate */

	public void generate(IGeneratorOutput output) throws IOException, GeneratorException
	{
		if (!m_schemaFile.exists())
		{
		String msg = m_schemaFile.getAbsolutePath()+" is missing";
		
			throw new GeneratorException(msg);		
		}
		
		FileUtils.forceMkdir(m_output);	
			
		compileClassList();

	} // generate
	
	/************************************************************/
	/* protected methods */
	/************************************************************/
	/** compileClassList */
	
	abstract protected void compileClassList() throws IOException, GeneratorException;
	
   	/************************************************************/
	/** getJavaClass */

    protected Class<?> getJavaClass(JavaClass javaClass)throws GeneratorException
    {
    	return getJavaClass(javaClass.getClassName());
    
    } // getJavaClass
    
	/************************************************************/
	/** getJavaClassList */
	
	protected JavaClassList getJavaClassList()throws GeneratorException
	{	
        try
        {
        FileReader fr = new FileReader(m_schemaFile);
 
        	return JavaClassList.unmarshalJavaClassList(fr);
        }
        catch (Throwable e)
        {
        String schemaFileName = m_schemaFile.getAbsolutePath();
        String msg = "Cannot read "+schemaFileName+": "+e.getMessage();
        	
        	throw new GeneratorException(msg);
        }
        	
	} // getJavaClassList

   	/************************************************************/
	// static public
   	/************************************************************/
	/** getJavaClass */

    static public Class<?> getJavaClass(String className)throws GeneratorException
    {    
    	try
    	{
			return Class.forName(className);		
		} 
    	catch (ClassNotFoundException e)
		{
    		throw new GeneratorException("Invalid class: "+className);		
		}   	
    	
    } // getJavaClass
	
} // JavaCompiler

/************************************************************/


