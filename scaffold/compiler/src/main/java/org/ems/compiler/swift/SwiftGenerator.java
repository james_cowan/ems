/************************************************************/
/*
	File: SwiftGenerator.java

	Author: James Cowan
*/
/************************************************************/

package org.ems.compiler.swift;

import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import org.ems.compiler.ClassGenerator;
import org.ems.compiler.model.JavaClass;
import org.ems.compiler.model.JavaClassList;
import org.ems.compiler.model.TypeDescriptorList;
import org.ems.generator.GeneratorException;

/************************************************************/

public class SwiftGenerator extends ClassGenerator
{	
	protected JavaClassList m_javaClassList;
	
	public SwiftGenerator(JavaClassList javaClassList, JavaClass javaClass, Class<?> cl, TypeDescriptorList typeDescriptorList)
	throws GeneratorException
	{
		super(javaClass, cl, typeDescriptorList);
		
		m_javaClassList = javaClassList;
	}

	/************************************************************/
	/* public methods */
	/************************************************************/
	/** generate */

	public void generate(PrintStream out) throws IOException, GeneratorException
	{
	
	} // generate
	
	/************************************************************/
	/* protected methods */
	/************************************************************/
	/** addImport */
	
	protected void addImport(String propertyImport, Map<String, String> importTable, List<String> importList)
	{
		if (propertyImport.equals("") || importTable.get(propertyImport) != null)
		{
			return;
		}
			
		importTable.put(propertyImport, propertyImport);
		importList.add(propertyImport);
	}
	
	protected JavaClass getJavaClass(Class<?>cl) throws GeneratorException 
	{
	String className = cl.getName();
	
		for (JavaClass javaClass : m_javaClassList.getJavaClass())
		{
			if (javaClass.getClassName().equals(className)) 
			{
				return javaClass;
			}
		}
		throw new GeneratorException("Unknown class "+className);
	}
	
	protected String getParmeterisedTypeName(PropertyDescriptor descriptor) 
	{
	Class<?> cl = descriptor.getPropertyType();
		
		if (cl.isArray())
		{
		Class<?> componentType = cl.getComponentType();
		String className = componentType.getSimpleName();
		
			return className+"[]";
		}
		
		if (cl == List.class)
		{
		Type type = descriptor.getReadMethod().getGenericReturnType();
		ParameterizedType pType = (ParameterizedType) type;
		Class<?> pClass = (Class<?>) pType.getActualTypeArguments()[0];
		
			return "Array<"+pClass.getSimpleName()+">";
		}
		
		return super.getPropertyTypeName(descriptor);
	}

} // SwiftGenerator

/************************************************************/


