/************************************************************/
/*
	File: CSharpCompiler.java

	Author: James Cowan
*/
/************************************************************/

package org.ems.compiler.csharp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;

import org.ems.compiler.ClassGenerator;
import org.ems.compiler.JavaCompiler;
import org.ems.compiler.model.JavaClass;
import org.ems.compiler.model.TypeDescriptor;
import org.ems.compiler.model.TypeDescriptorList;
import org.ems.generator.GeneratorException;

/************************************************************/

public class CSharpCompiler extends JavaCompiler
{
	protected TypeDescriptorList	m_typeDescriptorList;
	
	public CSharpCompiler(File schemaDirectory, File schemaFile, File output)
	throws GeneratorException
	{
		super(schemaDirectory, schemaFile, output);
		
		m_typeDescriptorList = getTypeDescriptorList();
	}

	/************************************************************/
	/* public methods */
	/************************************************************/

	
	/************************************************************/
	/* protected methods */
	/************************************************************/
	/** compileClassList */
	
	protected void compileClassList() throws IOException, GeneratorException
	{
	JavaClass javaClasses[] = m_javaClassList.getJavaClass();
	
		for (int count=0; count<javaClasses.length; count++)
		{
		JavaClass javaClass = javaClasses[count];
		Class<?> cl = getJavaClass(javaClass);
		File classFile = new File(m_output, cl.getSimpleName()+".cs");
		
			if (!cl.isEnum())
			{
			CSharpClassGenerator generator = new CSharpClassGenerator(m_javaClassList, javaClass, cl, m_typeDescriptorList);
				
				generate(classFile, generator);
			}
			else
			{
			CSharpEnumGenerator generator = new CSharpEnumGenerator(m_javaClassList, javaClass, cl, m_typeDescriptorList);
				
				generate(classFile, generator);
			}
		}
		
	} // compileClassList
	
	/************************************************************/
	/** generate */
	
	protected void generate(File file, ClassGenerator generator) throws IOException, GeneratorException
	{	
	FileOutputStream fos = new FileOutputStream(file);
	PrintStream ps = new PrintStream(fos);
			
		m_log.info("Create class: "+file.getAbsolutePath());
		
		generator.generate(ps);
		ps.close();
	}
	
	/************************************************************/
	/** getOutputDirectory */
	
	protected String getOutputDirectory(Class<?> cl)
	{
		if (m_javaClassList.getGeneratePackageDirectory())
		{
			return cl.getPackage().getName();
		}
		
		return m_javaClassList.getModuleDirectory();
	}
	
	/************************************************************/
	/** getTypeDescriptorList */
	
	protected TypeDescriptorList getTypeDescriptorList()throws GeneratorException
	{	
	String fileName = "/csharpTypes.xml";
	
        try
        {
        InputStream in = getClass().getResourceAsStream(fileName);
        InputStreamReader reader = new InputStreamReader(in);
		TypeDescriptorList typeDescriptorList = TypeDescriptorList.unmarshalTypeDescriptorList(reader);
		TypeDescriptor classTypeDescriptors[] = m_javaClassList.getTypeDescriptor();
		
			for (TypeDescriptor classTypeDescriptor : classTypeDescriptors)
			{
				typeDescriptorList.addTypeDescriptor(classTypeDescriptor);
			}
		
			return typeDescriptorList;
        }
        catch (Throwable e)
        {
        String msg = "Cannot read "+fileName+": "+e.getMessage();
        	
        	throw new GeneratorException(msg);
        }
        	
	} // getTypeDescriptorList
	
} // CSharpCompiler

/************************************************************/


