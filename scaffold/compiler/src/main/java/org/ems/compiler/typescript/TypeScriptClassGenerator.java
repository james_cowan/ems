/************************************************************/
/*
	File: TypeScriptClassGenerator.java

	TypeScriptClassGenerator generates as TypeScript class file from a java class.

	Author: James Cowan
*/
/************************************************************/

package org.ems.compiler.typescript;

import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.ems.compiler.ClassGenerator;
import org.ems.compiler.model.ImportDescriptor;
import org.ems.compiler.model.JavaClass;
import org.ems.compiler.model.JavaClassList;
import org.ems.compiler.model.TypeDescriptorList;
import org.ems.generator.GeneratorException;

/************************************************************/

public class TypeScriptClassGenerator extends TypeScriptGenerator
{	
	public TypeScriptClassGenerator(JavaClassList javaClassList, JavaClass javaClass, Class<?> cl, TypeDescriptorList typeDescriptorList)
	throws GeneratorException
	{
		super(javaClassList, javaClass, cl, typeDescriptorList);
	}

	/************************************************************/
	/* public methods */
	/************************************************************/
	/** generate */

	public void generate(PrintStream out) throws IOException, GeneratorException
	{
	List<PropertyDescriptor> propertyDescriptorList = getPropertyDescriptorList();
	HashMap<String, ImportDescriptor> importTable = new HashMap<String, ImportDescriptor>();
	ArrayList<ImportDescriptor> importList = new ArrayList<ImportDescriptor>();
	String extension = "";
	String className = m_class.getSimpleName();
	String instanceName = getInstanceName(className);
	boolean generateInterface = m_javaClass.getGenerateInterface();
	String interfaceName = (generateInterface) ? ClassGenerator.INTERFACE+className : "";
	String tab = "";

		if (!m_javaClassList.getGroupClasses())
		{
		ImportDescriptor classImports[] = m_javaClass.getImportDescriptor();
	
			for (ImportDescriptor classImport : classImports)
			{
				addImport(classImport, importTable, importList);
			}
		}
		
		if (generateInterface)
		{
		ImportDescriptor importDescriptor = new ImportDescriptor();
		
			importDescriptor.setTypeName(interfaceName);
			importDescriptor.setPath("./"+interfaceName);
		
			addImport(importDescriptor, importTable, importList);
		}
		
		for (int count=0; count<propertyDescriptorList.size(); count++)
		{
		PropertyDescriptor propertyDescriptor = propertyDescriptorList.get(count);
		PropertyType propertyType = getPropertyType(propertyDescriptor);
		String moduleName = propertyType.moduleName;
			
			if (!moduleName.equals(""))
			{
			ImportDescriptor importDescriptor = new ImportDescriptor();
				
				importDescriptor.setTypeName(moduleName);
				importDescriptor.setPath("./"+moduleName);
				addImport(importDescriptor, importTable, importList);
			}
		}
			
		if (hasSuperClass())
		{
		Class<?> superClass = m_class.getSuperclass();
		String superClassName = superClass.getSimpleName();
		
			extension = " extends "+superClassName;
			
			ImportDescriptor importDescriptor = new ImportDescriptor();
				
			importDescriptor.setTypeName(superClassName);
			importDescriptor.setPath("./"+superClassName);
			
			addImport(importDescriptor, importTable, importList);
		}
		
		if (!m_javaClassList.getGroupClasses())
		{
			for (ImportDescriptor importDescriptor : importList)
			{	
				out.println("import "+importDescriptor.getTypeName()+" from \""+importDescriptor.getPath()+"\";");
			}
			
			if (importList.size() > 0) 
			{
				out.println("");
			}
		}
		
		if (m_javaClassList.getGroupClasses()) 
		{
			out.print(tab+"export ");
		}
		else
		{
			out.print(tab+"export default ");
		}
		
		out.print("class "+className+extension);
		
		if (!interfaceName.equals(""))
		{
			out.print(" implements "+interfaceName);
		}
		
		out.println(" {");
		
		for (int count=0; count<propertyDescriptorList.size(); count++)
		{
		PropertyDescriptor propertyDescriptor = propertyDescriptorList.get(count);
		String name = propertyDescriptor.getName();
		PropertyType propertyType = getPropertyType(propertyDescriptor);
		
			out.println(tab+"\tpublic\t"+name+":"+propertyType.typeName+";");
		}
		out.println("");
		
		out.print(tab+"\tconstructor(");
		
		if (generateInterface)
		{
			out.print(instanceName+"?:"+interfaceName);
		}
		else
		{
			for (int count=0; count<propertyDescriptorList.size(); count++)
			{
			PropertyDescriptor propertyDescriptor = propertyDescriptorList.get(count);
			String name = propertyDescriptor.getName();
			PropertyType propertyType = getPropertyType(propertyDescriptor);
			
				if (count > 0)
				{
					out.print(", ");
				}
		
				out.print(name+":"+propertyType.typeName);
			}
		}
		
		out.println(") {");
		
		if (!extension.equals("")) 
		{
			String superArg = m_javaClass.getSuperArg();
			
			if (superArg.equals(""))
			{
				if (interfaceName.equals(""))
				{
					throw new GeneratorException(className+": Super argument expected when no interface generated");
				}
				superArg = instanceName;
			}
	
			out.println(tab+"\t\tsuper("+superArg+");"); 
			out.println("");
		} 
		
		if (generateInterface)
		{
			out.println(tab+"\t\tif ("+instanceName+") {"); 
			
			for (PropertyDescriptor propertyDescriptor : propertyDescriptorList)
			{
			String name = propertyDescriptor.getName();
		
				out.println(tab+"\t\t\tthis."+name+" = "+instanceName+"."+name+";");
			}
			out.println(tab+"\t\t}");
		}
		else
		{
			for (PropertyDescriptor propertyDescriptor : propertyDescriptorList)
			{
			String name = propertyDescriptor.getName();
		
				out.println(tab+"\t\tthis."+name+" = "+name+";");
			}
		}
		
		out.println(tab+"\t}");	
		out.println(tab+"}");
		
		if (m_javaClassList.getGroupClasses())
		{
			out.println("");
		}

	} // generate
	
} // TypeScriptClassGenerator

/************************************************************/


