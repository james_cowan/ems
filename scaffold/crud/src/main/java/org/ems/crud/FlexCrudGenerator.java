/************************************************************/
/*
	File: FlexCrudGenerator.java

	FlexCrudGenerator generates Flex CRUD

	Author: James Cowan
*/
/************************************************************/

package org.ems.crud;

import java.io.File;

import org.ems.generator.GeneratorException;

/************************************************************/

public class FlexCrudGenerator extends CrudGenerator
{
	public FlexCrudGenerator(File schemaDirectory, File schemaFile, File output)
	throws GeneratorException
	{
		super(schemaDirectory, schemaFile, output);	
	}

	/************************************************************/
	/* public methods */
	/************************************************************/

	/************************************************************/
	/* protected methods */
	/************************************************************/
	/** createUIGenerator */
	
	protected CrudUIGenerator	createUIGenerator()
	{
		return new FlexUIGenerator(m_output, m_crudServiceList);
		
	} // createUIGenerator
	
} // FlexCrudGenerator

/************************************************************/


