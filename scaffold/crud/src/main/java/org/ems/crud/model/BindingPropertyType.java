/*******************************************************************/
/*
	File: BindingPropertyType.java
*/
/*******************************************************************/

package org.ems.crud.model;

import java.beans.PropertyDescriptor;

/*******************************************************************/
/**
 * BindingPropertyType 
 *
 */
/*******************************************************************/

public class BindingPropertyType extends BindingProperty
{
	private static final long serialVersionUID = 1L;
	protected PropertyDescriptor		m_propertyDescriptor;

	public BindingPropertyType() {}
		
	public BindingPropertyType(BindingProperty bindingProperty)
	{
		setExcluded(bindingProperty.getExcluded());
		setFormExcluded(bindingProperty.getFormExcluded());
		setId(bindingProperty.getId());
		setJoin(bindingProperty.getJoin());
		setName(bindingProperty.getName());
		setReadOnly(bindingProperty.getReadOnly());
		setSortable(bindingProperty.getSortable());
		setTitle(bindingProperty.getTitle());
		setVisible(bindingProperty.getVisible());
		setWidth(bindingProperty.getWidth());
	}
	
	/*******************************************************************/
	/* public methods */
	/*******************************************************************/
	/** getPropertyDescriptor */
	
	public PropertyDescriptor getPropertyDescriptor()
	{
		return m_propertyDescriptor;
		
	} // getPropertyDescriptor
	
	/*******************************************************************/
	/** setPropertyDescriptor */
	
	public void setPropertyDescriptor(PropertyDescriptor propertyDescriptor)
	{
		m_propertyDescriptor = propertyDescriptor;
		
	} // setPropertyDescriptor
	
} // BindingPropertyType

/*******************************************************************/


