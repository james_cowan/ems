/************************************************************/
/*
	File: FlexUIGenerator.template

	generates a Flex UI

	Author: James Cowan
*/
/************************************************************/

package org.ems.crud;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.ems.crud.model.CrudServiceList;
import org.ems.generator.GeneratorException;

/************************************************************/

public class FlexUIGenerator extends CrudUIGenerator
{	
	static protected Logger		m_log = Logger.getLogger(FlexUIGenerator.class);
	
	static final public String	AS = ".as";
	static final public String	MXML = ".mxml";
	
	public FlexUIGenerator(File outputDir, CrudServiceList crudServiceList)
	{
		super(outputDir, crudServiceList);
	}

	/************************************************************/
	/* public methods */
	/************************************************************/
	/** generate */

	public void generate() throws IOException, GeneratorException
	{		
		getApplicationGenerator().generate();
		getControllerGenerator().generate();
		getViewGenerator().generate();
		
	} // generate
	
	/************************************************************/
	/* protected methods */
	/************************************************************/
	/** getApplicationGenerator */
	
	protected CrudUIGenerator getApplicationGenerator()
	{
		return new FlexApplicationGenerator(m_output, m_crudServiceList);
		
	} // getApplicationGenerator
	
	/************************************************************/
	/** getControllerGenerator */
	
	protected CrudUIGenerator getControllerGenerator()
	{
		return new FlexControllerGenerator(m_output, m_crudServiceList);
		
	} // getControllerGenerator
	
	/************************************************************/
	/** getViewGenerator */
	
	protected CrudUIGenerator getViewGenerator()
	{
		return new FlexViewGenerator(m_output, m_crudServiceList);
		
	} // getViewGenerator
	
} // FlexUIGenerator

/************************************************************/


