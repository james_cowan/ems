/*******************************************************************/
/*
	File: TemplateException.java

*/
/*******************************************************************/

package org.ems.template;

/*******************************************************************/

public class TemplateException extends Exception
{
	private static final long serialVersionUID = 1L;

	public TemplateException(String msg)
	{
		super(msg);
	}

} // TemplateException

/*******************************************************************/

