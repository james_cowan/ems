/************************************************************/
/*
	File: SQLType.java

	SQL Type enumeration
	
	Author: James Cowan
*/
/************************************************************/

package org.ems.orm;

public enum SQLType
{
	BIGINT,
	INT,
	VARCHAR
}
