/*******************************************************************/
/*
	File: DaoNotFoundException.java
	
	Author: James Cowan
*/
/*******************************************************************/

package org.ems.dao;

/*******************************************************************/
/** DaoNotFoundException class */

public class DaoNotFoundException extends Exception
{
	private static final long serialVersionUID = 1L;

	public DaoNotFoundException(String msg)
	{
		super(msg);	
	}
	
} // DaoNotFoundException

/*******************************************************************/

