/*******************************************************************/
/*
	File: DaoException.java
	
	Author: James Cowan
*/
/*******************************************************************/

package org.ems.dao;

/*******************************************************************/
/** DaoException class */

public class DaoException extends Exception
{
	private static final long serialVersionUID = 1L;

	public DaoException(String msg)
	{
		super(msg);	
	}
	
} // DaoException

/*******************************************************************/

