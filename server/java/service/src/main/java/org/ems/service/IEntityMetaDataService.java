/************************************************************/
/*
	File: EntityMetaDataService.java

	Author: James Cowan
*/
/************************************************************/

package org.ems.service;

import java.util.List;

import org.ems.dao.DaoException;
import org.ems.dao.types.EntityMetaData;
import org.ems.dao.types.EntityPropertyMetaData;

public interface IEntityMetaDataService
{	
	/************************************************************/
	// public methods
	/************************************************************/
	/** getEntityMetaData */
	
	public List<EntityMetaData> getEntityMetaData();
	
	/************************************************************/
	/** getPrimaryKey */
	
	public EntityPropertyMetaData getPrimaryKey(String entityName)
	throws DaoException;
	
	/************************************************************/
	/** getProperty */
	
	public EntityPropertyMetaData getEntityPropertyMetaData(String entityName, String propertyName)
	throws DaoException;
	
} // IEntityMetaDataService

/************************************************************/
// End of class
/************************************************************/
