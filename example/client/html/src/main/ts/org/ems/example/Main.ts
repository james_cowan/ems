/*******************************************************************/
/*
	File: Main.js
*/
/*******************************************************************/
///<reference path='./model/Album.ts'/>
///<reference path='./model/IAlbum.ts'/>
///<reference path='./model/Artist.ts'/>

import Album = require("model/Album");
import IAlbum = require("model/IAlbum");
import Artist = require("model/Artist");

module org.ems.example
{
    export class Main
    {
        public album:Album;

        constructor()
        {
            this.album = new Album();
            this.album.id = 1;
            this.album.artist = new Artist();
            this.album.artist.name = "Jimi Hendrix";
            this.album.name = "Electric Ladyland";
        }

        public fromJSON(json:string):Album
        {
            var data:IAlbum = JSON.parse(json);

            return new Album(data);
        }

        public toJSON(album:Album):string
        {
            return JSON.stringify(album);
        }
    }
}

/*******************************************************************/
